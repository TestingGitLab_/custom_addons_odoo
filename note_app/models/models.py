# -*- coding: utf-8 -*-

from odoo import fields, models


class NoteApp(models.Model):
    _name = "app.note"
    _description = "Note app"

    name = fields.Char()
    description = fields.Text()
