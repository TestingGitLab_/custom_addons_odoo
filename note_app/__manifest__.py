# -*- coding: utf-8 -*-
{
    "name": "note_app",
    "summary": """
        Note app
    """,
    "description": """
       Note module
    """,
    "author": "Manuel Pita",
    "website": "http://manuelpita.com",
    "category": "App Custom",
    "version": "0.1",
    "depends": ["base"],
    "data": ["security/ir.model.access.csv", "views/views.xml"],
    "demo": ["demo/demo.xml"],
    "application": True,
}
