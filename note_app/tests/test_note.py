from odoo.tests import TransactionCase, tagged


@tagged("-standard", "post_install", "custom")
class TestModelNoteApp(TransactionCase):
    def test_some_action(self):
        record = self.env["app.note"].create(
            {"name": "Test name", "description": "Test description"}
        )
        self.assertEqual(record.name, "Test name")
        self.assertEqual(record.description, "Test description")
        print("Test is successful")
