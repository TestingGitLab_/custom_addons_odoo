import subprocess
import sys


def system(*args, **kwargs):
    kwargs.setdefault("stdout", subprocess.PIPE)
    proc = subprocess.Popen(args, **kwargs)
    out, err = proc.communicate()
    return out


def main():
    args_test = [
        "../odoo/odoo-bin",  # path of your odoo-init from this file
        "-c" "odoo.conf",
        "-d",
        "odoo",  # dbname use
        "--xmlrpc-port=8070",
        "--log-level=error",
        "--test-enable",
        "--test-tags",
        "-standard,custom",
        "--stop-after-init",
    ]
    output_test = system(*args_test, stderr=subprocess.STDOUT)
    decode_output_test = output_test.decode("utf-8")

    if "FAIL:" in decode_output_test:
        print(decode_output_test)
        print(
            u"Testing error have been detected.  Please fix them\n"
            'or force the commit with "git commit --no-verify".\n'
        )
        sys.exit(1)


if __name__ == "__main__":
    main()
